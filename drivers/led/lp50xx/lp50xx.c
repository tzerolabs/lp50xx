/*
 * Copyright (c) 2020 Seagate Technology LLC
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#define DT_DRV_COMPAT ti_lp50xx

/**
 * @file
 * @brief lp50xx LED controller
 */

#include <drivers/i2c.h>
#include <drivers/led.h>
#include <device.h>
#include <zephyr.h>

#define LOG_LEVEL CONFIG_LED_LOG_LEVEL
#include <logging/log.h>

LOG_MODULE_REGISTER(lp50xx);

#define LP50XX_DEVICE_CONFIG0		0X00
#define   CONFIG0_CHIP_EN		BIT(6)

#define LP50XX_DEVICE_CONFIG1		0x01
#define   CONFIG1_LED_GLOBAL_OFF	BIT(0)
#define   CONFIG1_MAX_CURRENT_OPT	BIT(1)
#define   CONFIG1_PWM_DITHERING_EN	BIT(2)
#define   CONFIG1_AUTO_INCR_EN		BIT(3)
#define   CONFIG1_POWER_SAVE_EN		BIT(4)
#define   CONFIG1_LOG_SCALE_EN		BIT(5)

#define LP50XX_LED_CONFIG0		0x02
#define   CONFIG0_LED0_BANK_EN		BIT(0)
#define   CONFIG0_LED1_BANK_EN		BIT(1)
#define   CONFIG0_LED2_BANK_EN		BIT(2)
#define   CONFIG0_LED3_BANK_EN		BIT(3)
#define   CONFIG0_LED4_BANK_EN		BIT(4)
#define   CONFIG0_LED5_BANK_EN		BIT(5)
#define   CONFIG0_LED6_BANK_EN		BIT(6)
#define   CONFIG0_LED7_BANK_EN		BIT(7)
//lp503x variants
#define LP50XX_LED_CONFIG1		0x03
#define   CONFIG1_LED8_BANK_EN		BIT(0)
#define   CONFIG1_LED9_BANK_EN		BIT(1)
#define   CONFIG1_LED10_BANK_EN		BIT(2)
#define   CONFIG1_LED11_BANK_EN		BIT(3)


struct lp50xx_config {
	bool log_scale_en;
	bool max_curr_opt;
	uint8_t reset_addr;
	uint8_t num_leds;
	const char *i2c_bus_label;
	const struct led_info *leds_info;
	uint8_t i2c_addr;
};

struct lp50xx_variant {
	uint32_t num_leds;
	uint32_t bank_base;
	uint32_t led_brightness_base;
	uint32_t led_color_base;
	uint8_t reset_addr;
};

struct lp50xx_data{
	const struct lp50xx_variant *variant;
	const struct device *i2c;
};

enum lp50xx_variants {

	lp5018,
	lp5024,
	lp5030,
	lp5036,
    lp5012,
	lp5009

};

#define VARIANT(_num_leds, _bank_base, 				\
		_led_brightness_base, _led_color_base, _reset_addr)\
{ 								\
	.num_leds=_num_leds,.bank_base=_bank_base, 		\
	.led_brightness_base=_led_brightness_base, 		\
	.led_color_base=_led_color_base, 			\
	.reset_addr=_reset_addr, 	 			\
},

const struct lp50xx_variant variants[] = {

	[lp5009] = VARIANT(9, 0x03, 0x07, 0x0B, 0x17)
	[lp5012] = VARIANT(12, 0x03, 0x07, 0x0B, 0x17)
	[lp5018] = VARIANT(18, 0x03, 0x07, 0x0F, 0x27)
	[lp5024] = VARIANT(24, 0x03, 0x07, 0x0F, 0x27)
	[lp5030] = VARIANT(30, 0x04, 0x08, 0x14, 0x38)
	[lp5036] = VARIANT(36, 0x04, 0x08, 0x14, 0x38)
};


#define DEV_CFG(dev)	((const struct lp50xx_config *)((dev)->config))
#define DEV_DATA(dev)	((struct lp50xx_data *) ((dev)->data))

static const struct led_info *
lp50xx_led_to_info(const struct device *dev, uint32_t led)
{
	int i;
	const struct lp50xx_config *cfg = DEV_CFG(dev);
	const struct lp50xx_data *data = DEV_DATA(dev);

	for (i = 0; i < data->variant->num_leds; i++) {
		if (cfg->leds_info[i].index == led) {
			return &cfg->leds_info[i];
		}
	}
	return NULL;
}

static int lp50xx_get_info(const struct device *dev, uint32_t led,
			   const struct led_info **info)
{
	const struct led_info *led_info = lp50xx_led_to_info(dev, led);

	if (!led_info) {
		return -EINVAL;
	}

	*info = led_info;

	return 0;
}

static int lp50xx_write_reg(const struct device *dev, uint8_t reg, uint8_t data)
{
	uint8_t buf[2] = {reg, data};
	return i2c_write(DEV_DATA(dev)->i2c, buf, 2, DEV_CFG(dev)->i2c_addr);
}

static int lp50xx_set_brightness(const struct device *dev,
				 uint32_t led, uint8_t value)
{
	const struct lp50xx_data *data = DEV_DATA(dev);
	const struct led_info *info = lp50xx_led_to_info(dev, led);


	
	uint8_t addr= data->variant->led_brightness_base + info->index;
	uint8_t val = value;

	return lp50xx_write_reg(dev, addr, val);
}

static int lp50xx_on(const struct device *dev, uint32_t led)
{
	return lp50xx_set_brightness(dev, led, 255);
}

static int lp50xx_off(const struct device *dev, uint32_t led)
{
	return lp50xx_set_brightness(dev, led, 0);
}

static int lp50xx_set_color(const struct device *dev, uint32_t led,
			    uint8_t num_colors, const uint8_t *color)
{
	const struct lp50xx_config *cfg = DEV_CFG(dev);
	struct lp50xx_data *data = DEV_DATA(dev);
	const struct led_info *led_info = lp50xx_led_to_info(dev, led);

	if (!led_info || num_colors != led_info->num_colors) {
		return -EINVAL;
	}

	for(int i=0;i<num_colors;i++)
	{
		if(led_info->color_mapping[i] >= num_colors)
		{
			LOG_ERR("Color mapping index out of range. Value of %d is great than max of %d",
					led_info->color_mapping[i],
					num_colors);
		}
	}

	uint8_t buf[] = {
		data->variant->led_color_base + (num_colors * led_info->index),
		color[led_info->color_mapping[0]],
		color[led_info->color_mapping[1]],
		color[led_info->color_mapping[2]]
	};

	return i2c_write(data->i2c, buf, ARRAY_SIZE(buf), cfg->i2c_addr);
}

static int lp50xx_enable(const struct device *dev)
{
	return lp50xx_write_reg(dev, 0, CONFIG0_CHIP_EN);
}

static int lp50xx_reset(const struct device *dev)
{
	return lp50xx_write_reg(dev, DEV_CFG(dev)->reset_addr, 0xFF);
}

static int lp50xx_init(const struct device *dev)
{
	struct lp50xx_data *data = (struct lp50xx_data*)dev->data;
	const struct lp50xx_config *cfg = DEV_CFG(dev);
	int err;

	LOG_DBG("Initializing...");

	data->i2c = device_get_binding(cfg->i2c_bus_label);
	if(!data->i2c){
		return -ENODEV;
	}

	err = lp50xx_reset(dev);
	if(err){
		return err;
	}

	err = lp50xx_enable(dev);
	if (err < 0) {
		return err;
	}

	/* Apply configuration. */
	uint8_t config1_reg = 
			CONFIG1_PWM_DITHERING_EN | 
			CONFIG1_AUTO_INCR_EN | 
			CONFIG1_POWER_SAVE_EN;
	if (cfg->max_curr_opt) {
		config1_reg |= CONFIG1_MAX_CURRENT_OPT;
	}
	if (cfg->log_scale_en) {
		config1_reg |= CONFIG1_LOG_SCALE_EN;
	}

	return lp50xx_write_reg(dev, LP50XX_LED_CONFIG1, config1_reg);
}

static const struct led_driver_api lp50xx_led_api = {
	.on		= lp50xx_on,
	.off		= lp50xx_off,
	.get_info	= lp50xx_get_info,
	.set_brightness	= lp50xx_set_brightness,
	.set_color	= lp50xx_set_color,
};


#define COLOR_MAPPING(led_node_id)				\
const uint8_t color_mapping_##led_node_id[] =			\
		DT_PROP(led_node_id, color_mapping);

#define LED_INFO(led_node_id)					\
{								\
	.label = DT_LABEL(led_node_id), 			\
	.index = DT_PROP(led_node_id, index),			\
	.num_colors = DT_PROP_LEN(led_node_id, color_mapping),	\
	.color_mapping	= color_mapping_##led_node_id,		\
},

#define lp50xx_DEVICE(id)					\
								\
DT_INST_FOREACH_CHILD(id, COLOR_MAPPING)			\
								\
const struct led_info lp50xx_leds_##id[] = {			\
	DT_INST_FOREACH_CHILD(id, LED_INFO)			\
};								\
								\
static struct lp50xx_config lp50xx_config_##id = {		\
	.i2c_bus_label	= DT_INST_BUS_LABEL(id),		\
	.i2c_addr	= DT_INST_REG_ADDR(id),			\
	.num_leds	= ARRAY_SIZE(lp50xx_leds_##id),		\
	.max_curr_opt	= DT_INST_PROP(id, max_curr_opt),	\
	.log_scale_en	= DT_INST_PROP(id, log_scale_en),	\
	.leds_info	= lp50xx_leds_##id,			\
};								\
								\
static struct lp50xx_data lp50xx_data_##id = {			\
	.variant = &variants[DT_INST_PROP(id, variant)], 	\
};								\
								\
DEVICE_DT_INST_DEFINE(id, lp50xx_init,				\
		    NULL,			\
		    &lp50xx_data_##id,				\
		    &lp50xx_config_##id,			\
		    POST_KERNEL, CONFIG_LED_INIT_PRIORITY,	\
		    &lp50xx_led_api);

DT_INST_FOREACH_STATUS_OKAY(lp50xx_DEVICE)
